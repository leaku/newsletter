<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210604122922 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE entry (id INT AUTO_INCREMENT NOT NULL, newsletter_id INT NOT NULL, submitted_at DATETIME NOT NULL, stud_org VARCHAR(255) NOT NULL, organisation VARCHAR(255) NOT NULL, submitter VARCHAR(255) NOT NULL, submitter_email VARCHAR(255) NOT NULL, title VARCHAR(255) NOT NULL, start_at DATETIME DEFAULT NULL, end_at DATETIME DEFAULT NULL, place VARCHAR(255) DEFAULT NULL, description LONGTEXT NOT NULL, title_en VARCHAR(255) NOT NULL, description_en LONGTEXT NOT NULL, category VARCHAR(255) NOT NULL, category_en VARCHAR(255) NOT NULL, is_checked TINYINT(1) NOT NULL, INDEX IDX_2B219D7022DB1917 (newsletter_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE newsletter (id INT AUTO_INCREMENT NOT NULL, release_date DATE NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE entry ADD CONSTRAINT FK_2B219D7022DB1917 FOREIGN KEY (newsletter_id) REFERENCES newsletter (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE entry DROP FOREIGN KEY FK_2B219D7022DB1917');
        $this->addSql('DROP TABLE entry');
        $this->addSql('DROP TABLE newsletter');
    }
}
