<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210602091300 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE entry (id INT AUTO_INCREMENT NOT NULL, submitted_at DATETIME NOT NULL, stud_org VARCHAR(255) NOT NULL, organisation VARCHAR(255) NOT NULL, submitter VARCHAR(255) NOT NULL, submtiter_email VARCHAR(255) NOT NULL, title VARCHAR(255) NOT NULL, start_at DATETIME DEFAULT NULL, end_at DATETIME DEFAULT NULL, place VARCHAR(255) DEFAULT NULL, description LONGTEXT NOT NULL, title_en VARCHAR(255) NOT NULL, description_en LONGTEXT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE entry');
    }
}
