<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SubmitCSVType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
          ->add('newsletterAt', DateType::class, [
              'widget' => 'single_text',
              'label' => 'Newsletter Release Date',
          ])
          ->add('filename', FileType::class, [
              'mapped' => false ,
              'attr' => [
                'placeholder' => 'Select a File',
                'accept' => 'text/csv,.tsv',
              ],
          ])
          ->add('Upload', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
