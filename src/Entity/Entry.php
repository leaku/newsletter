<?php

namespace App\Entity;

use App\Repository\EntryRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=EntryRepository::class)
 */
class Entry
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $SubmittedAt;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $StudOrg;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Organisation;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Submitter;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $SubmitterEMail;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Title;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $StartAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $EndAt;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Place;

    /**
     * @ORM\Column(type="text")
     */
    private $Description;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $TitleEN;

    /**
     * @ORM\Column(type="text")
     */
    private $DescriptionEN;

    /**
     * @ORM\ManyToOne(targetEntity=Newsletter::class, inversedBy="entries")
     * @ORM\JoinColumn(nullable=false)
     */
    private $Newsletter;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Category;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $CategoryEN;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isChecked;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSubmittedAt(): ?\DateTimeInterface
    {
        return $this->SubmittedAt;
    }

    public function setSubmittedAt(\DateTimeInterface $SubmittedAt): self
    {
        $this->SubmittedAt = $SubmittedAt;

        return $this;
    }

    public function getStudOrg(): ?string
    {
        return $this->StudOrg;
    }

    public function setStudOrg(string $StudOrg): self
    {
        $this->StudOrg = $StudOrg;

        return $this;
    }

    public function getOrganisation(): ?string
    {
        return $this->Organisation;
    }

    public function setOrganisation(string $Organisation): self
    {
        $this->Organisation = $Organisation;

        return $this;
    }

    public function getSubmitter(): ?string
    {
        return $this->Submitter;
    }

    public function setSubmitter(string $Submitter): self
    {
        $this->Submitter = $Submitter;

        return $this;
    }

    public function getSubmitterEMail(): ?string
    {
        return $this->SubmitterEMail;
    }

    public function setSubmitterEMail(string $SubmitterEMail): self
    {
        $this->SubmitterEMail = $SubmitterEMail;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->Title;
    }

    public function setTitle(string $Title): self
    {
        $this->Title = $Title;

        return $this;
    }

    public function getStartAt(): ?\DateTimeInterface
    {
        return $this->StartAt;
    }

    public function setStartAt(?\DateTimeInterface $StartAt): self
    {
        $this->StartAt = $StartAt;

        return $this;
    }

    public function getEndAt(): ?\DateTimeInterface
    {
        return $this->EndAt;
    }

    public function setEndAt(?\DateTimeInterface $EndAt): self
    {
        $this->EndAt = $EndAt;

        return $this;
    }

    public function getPlace(): ?string
    {
        return $this->Place;
    }

    public function setPlace(?string $Place): self
    {
        $this->Place = $Place;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->Description;
    }

    public function setDescription(string $Description): self
    {
        $this->Description = $Description;

        return $this;
    }

    public function getTitleEN(): ?string
    {
        return $this->TitleEN;
    }

    public function setTitleEN(string $TitleEN): self
    {
        $this->TitleEN = $TitleEN;

        return $this;
    }

    public function getDescriptionEN(): ?string
    {
        return $this->DescriptionEN;
    }

    public function setDescriptionEN(string $DescriptionEN): self
    {
        $this->DescriptionEN = $DescriptionEN;

        return $this;
    }

    public function getNewsletter(): ?Newsletter
    {
        return $this->Newsletter;
    }

    public function setNewsletter(?Newsletter $Newsletter): self
    {
        $this->Newsletter = $Newsletter;

        return $this;
    }

    public function getCategory(): ?string
    {
        return $this->Category;
    }

    public function setCategory(string $Category): self
    {
        $this->Category = $Category;

        return $this;
    }

    public function getCategoryEN(): ?string
    {
        return $this->CategoryEN;
    }

    public function setCategoryEN(string $CategoryEN): self
    {
        $this->CategoryEN = $CategoryEN;

        return $this;
    }

    public function getIsChecked(): ?bool
    {
        return $this->isChecked;
    }

    public function setIsChecked(bool $isChecked): self
    {
        $this->isChecked = $isChecked;

        return $this;
    }
}
