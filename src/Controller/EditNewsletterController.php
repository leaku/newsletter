<?php

namespace App\Controller;

use App\Repository\EntryRepository;
use App\Repository\NewsletterRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\ByteString;
use Symfony\Component\String\CodePointString;
use Symfony\Component\String\UnicodeString;

class EditNewsletterController extends AbstractController
{
    /**
     * @Route("/edit/{id}", name="edit_newsletter")
     */
    public function index(NewsletterRepository $newsletterRepository, int $id): Response
    {
        $newsletter = $newsletterRepository->findBy(array('id' => $id))[0];
        $entries = $newsletter->getEntries()->toArray();
;
        usort($entries, function ($a, $b){
          $eventTypes = array(
            'VSETH' => 0,
            'EVENTS' => 1,
            'SONSTIGES' => 2,
            'EXTERN' => 3
          );
          $orgTypes = array(
            'VSETH' => 0,
            'Kommission' => 1,
            'Fachverein' => 2,
            'Assoziierte Organisation' => 3,
            'Anerkannte Organisation' => 4,
            'ETH' => 5,
            'Extern' => 6,
          );
          if($eventTypes[$a->getCategory()] == $eventTypes[$b->getCategory()]) {
            if($orgTypes[$a->getStudOrg()] == $orgTypes[$b->getStudOrg()]) {
              if($a->getOrganisation() == $b->getOrganisation()){
                if($a->getTitle() == $b->getTitle()){
                  return 0;    
                } else if (strcmp($a->getTitle(), $b->getTitle() > 0)) {
                  return -1;
                } else {
                  return 1;
                }
              } else if (strcmp($a->getOrganisation(), $b->getOrganisation()) > 0){
                return 1;
              } else {
                return -1;
              }
            } else if ($orgTypes[$a->getStudOrg()] > $orgTypes[$b->getStudOrg()]) {
              return 1;
            } else {
              return -1;
            }
          } else if($eventTypes[$a->getCategory()] > $eventTypes[$b->getCategory()]) {
            return 1;
          } else {
            return -1;
          } 
        });

        return $this->render('edit_newsletter/index.html.twig', [
            'newsletter' => $newsletter,
            'entries' => $entries,
        ]);
    }

    /**
     * @Route("/entry/remove/{idstr}", name="entry_remove")
     */
    public function remove(EntryRepository $entryRepository, string $idstr) {
      $nid = explode('_', $idstr)[0];
      $id = explode('_', $idstr)[1];
      $entry = $entryRepository->findBy(array('id' => $id))[0];
      $em = $this->getDoctrine()->getManager();

      $em->remove($entry);
      $em->flush();

      return $this->redirect($this->generateUrl('edit_newsletter', ['id' => $nid]));
    }
}
