<?php

namespace App\Controller;

use App\Form\EditEntryType;
use App\Repository\EntryRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class EditEntryController extends AbstractController
{
    /**
     * @Route("/editEntry/{idstr}", name="edit_entry")
     */
    public function index(EntryRepository $entryRepository, string $idstr, Request $request): Response
    {
        $id = explode('_', $idstr)[1];
        $nid = explode('_', $idstr)[0];
        $entry = $entryRepository->findBy(array('id' => $id))[0];

        $form = $this->createForm(EditEntryType::class, $entry);

        $form->handleRequest($request);

        if($form->isSubmitted()) {
          $em = $this->getDoctrine()->getManager();

          $entry->setIsChecked(true);

	  $entry->setCategoryEN($entry->getCategory());

	  if($entry->getCategoryEN() == "SONSTIGES")
	    $entry->setCategoryEN("OTHER");
	  else if ($entry->getCategoryEN() == "EXTERN")
	    $entry->setCategoryEN("EXTERNAL");

          $em->flush();

          return $this->redirect($this->generateUrl('edit_newsletter', ['id' => $nid]));
        }


        return $this->render('edit_entry/index.html.twig', [
            'entry' => $entry,
            'form' => $form->createView(),
        ]);
    }
}

