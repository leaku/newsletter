<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class JPController extends AbstractController
{
    /**
     * @Route("/j/p", name="j_p")
     */
    public function index(): Response
    {
        return $this->render('jp/index.html.twig', [
            'controller_name' => 'JPController',
        ]);
    }
}
